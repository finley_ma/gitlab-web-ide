import type { BaseConfig, VSCodeTrackingMessage, TrackingEvent } from '@gitlab/web-ide-types';

const mapVSCodeTrackingEventToIDETrackingEvent = (
  event: VSCodeTrackingMessage['params']['event'],
): TrackingEvent | null => {
  switch (event.name) {
    case 'remoteConnectionSuccess':
      return { name: 'remote-connection-success' };
    case 'remoteConnectionFailure':
      return { name: 'remote-connection-failure' };
    default:
      return null;
  }
};

export default function handleVSCodeTrackingMessage(
  params: VSCodeTrackingMessage['params'],
  config: BaseConfig,
) {
  const trackingEvent = mapVSCodeTrackingEventToIDETrackingEvent(params.event);

  if (trackingEvent !== null) {
    config.handleTracking?.(trackingEvent);
  }
}
