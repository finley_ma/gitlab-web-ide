import { AnyConfig, ClientOnlyConfig } from '@gitlab/web-ide-types';
import { start, startRemote } from './index';
import { getIframeHtml } from './getIframeHtml';
import { createUnloadPreventer, IUnloadPreventer } from './unloadPreventer';
import handleVSCodeTrackingMessage from './handleVSCodeTrackingMessage';
import { createFullConfig, createClientOnlyConfig } from './test-utils/mockConfig';

jest.mock('./getIframeHtml');
jest.mock('./unloadPreventer');
jest.mock('./handleVSCodeTrackingMessage');

describe('web-ide/src/index', () => {
  let parentElement: Element;
  let unloadPreventerMock: IUnloadPreventer;
  const config: AnyConfig = createFullConfig();
  const clientOnlyConfig: ClientOnlyConfig = createClientOnlyConfig();
  const iframeHtml = '<html><body>web ide assets</body></html>';
  const getIframe = () => document.querySelector('iframe');
  const postMessage = (key: string, params = {}) => {
    getIframe()?.contentWindow?.postMessage({ key, params }, window.location.origin);
  };

  const waitUntilMockIsInvoked = (mockFn: jest.Mock) =>
    new Promise<void>(resolve => {
      mockFn.mockImplementationOnce(() => resolve());
    });

  beforeEach(() => {
    parentElement = document.createElement('div');

    document.body.append(parentElement);
  });

  beforeEach(() => {
    unloadPreventerMock = {
      setShouldPrevent: jest.fn(),
      dispose: jest.fn(),
    };
    (createUnloadPreventer as jest.Mock).mockReturnValueOnce(unloadPreventerMock);
  });

  afterEach(() => {
    parentElement.remove();
  });

  describe.each`
    startFn        | methodName
    ${start}       | ${'start'}
    ${startRemote} | ${'startRemote'}
  `('$methodName', ({ startFn }) => {
    let dispose: () => void;

    beforeEach(() => {
      (getIframeHtml as jest.Mock).mockReturnValueOnce(iframeHtml);
      ({ dispose } = startFn(parentElement, config));
    });

    it('creates an iframe that hosts the Web IDE', () => {
      expect(getIframe()).not.toBe(null);
    });

    it('adds the iframe HTML to the iframe', () => {
      expect(getIframe()?.contentWindow?.document.body.innerHTML).toBe('web ide assets');
    });

    it.each`
      message           | params       | handler                     | handlerName
      ${'start-remote'} | ${{}}        | ${config.handleStartRemote} | ${'handleStartRemote'}
      ${'error'}        | ${{}}        | ${config.handleError}       | ${'handleError'}
      ${'close'}        | ${undefined} | ${config.handleClose}       | ${'handleClose'}
    `(
      'calls $handlerName when iframe posts $message message',
      async ({ message, params, handler }) => {
        postMessage(message, params);

        await waitUntilMockIsInvoked(handler);

        if (params) {
          expect(handler).toHaveBeenCalledWith(params);
        } else {
          expect(handler).toHaveBeenCalled();
        }
      },
    );

    describe('when iframe receives web-ide-tracking message', () => {
      it('calls handleTracking handler and passes the message event parameter', async () => {
        const params = { event: { name: 'connect-to-remote' } };

        postMessage('web-ide-tracking', params);

        await waitUntilMockIsInvoked(config.handleTracking as jest.Mock);

        expect(config.handleTracking).toHaveBeenCalledWith(params.event);
      });
    });

    describe('when iframe receives vscode-tracking message', () => {
      it('calls handleVSCodeTrackingEvent function', async () => {
        const params = { event: { name: 'remote-connection-successful' } };

        postMessage('vscode-tracking', params);

        await waitUntilMockIsInvoked(handleVSCodeTrackingMessage as jest.Mock);

        expect(handleVSCodeTrackingMessage).toHaveBeenCalledWith(params, config);
      });
    });

    describe('on dispose', () => {
      it('removes iframe', () => {
        expect(getIframe()).not.toBe(null);

        dispose();

        expect(getIframe()).toBe(null);
      });

      it('disposes unloadPreventer', () => {
        dispose();

        expect(unloadPreventerMock.dispose).toHaveBeenCalled();
      });
    });
  });

  describe('start', () => {
    let ready: Promise<void>;

    beforeEach(() => {
      ({ ready } = start(parentElement, clientOnlyConfig));
    });

    it('resolves "ready" when "ready" message has been posted', async () => {
      const isReadySpy = jest.fn();

      // eslint-disable-next-line @typescript-eslint/no-floating-promises
      ready.then(isReadySpy);

      expect(isReadySpy).not.toHaveBeenCalled();

      postMessage('ready');

      await expect(ready).resolves.toBeUndefined();
      expect(isReadySpy).toHaveBeenCalled();
    });
  });

  describe('prevent-unload message', () => {
    beforeEach(() => {
      start(parentElement, clientOnlyConfig);
    });

    it.each([true, false])('updates uploadPreventer shouldPrevent state', async shouldPrevent => {
      postMessage('prevent-unload', {
        shouldPrevent,
      });

      await waitUntilMockIsInvoked(unloadPreventerMock.setShouldPrevent as jest.Mock);

      expect(unloadPreventerMock.setShouldPrevent).toHaveBeenCalledWith(shouldPrevent);
    });
  });
});
