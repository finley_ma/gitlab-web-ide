import { joinPaths } from '@gitlab/utils-path';
import { FileListWithCache } from './FileListWithCache';
import { FileStatusType, IFileList, IFileSystem, ISourceControlSystem } from './types';

interface FileListOptions {
  readonly initBlobs: string[];
  readonly sourceControl: ISourceControlSystem;
}
/**
 * This provides an unordered list of all blobs from the given "fs" relative to the "repoPath"
 */
export class FileList implements IFileList {
  private readonly _initBlobsWithoutRoot: string[];

  private readonly _sourceControl: ISourceControlSystem;

  constructor({ initBlobs, sourceControl }: FileListOptions) {
    // why: We want to clone the array given to us + also make sure all paths are absolute
    this._initBlobsWithoutRoot = initBlobs.map(x => joinPaths('/', x));
    this._sourceControl = sourceControl;
  }

  async listAllBlobs(): Promise<string[]> {
    const status = await this._sourceControl.status();

    const deletedFiles = new Set(
      status.filter(x => x.type === FileStatusType.Deleted).map(x => x.path),
    );
    const addedFiles = status.filter(x => x.type === FileStatusType.Created).map(x => x.path);

    return this._initBlobsWithoutRoot.filter(x => !deletedFiles.has(x)).concat(addedFiles);
  }

  /**
   * Returns a cached version of this FileList
   *
   * @param fs used to determine if the cache needs to be invalidated
   * @returns
   */
  withCache(fs: IFileSystem): IFileList {
    return new FileListWithCache(this, fs);
  }
}
