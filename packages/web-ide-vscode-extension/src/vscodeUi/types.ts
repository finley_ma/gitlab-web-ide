export interface IInputCanceled {
  canceled: true;
}

export interface IInputAccepted<T> {
  canceled: false;
  value: T;
}

export type IInputResponse<T> = IInputAccepted<T> | IInputCanceled;
